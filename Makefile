# All purpose variables
compiler=g++
debugger=/usr/local/bin/gdb
project_root=./

$(warning project root is $project_root)
#   Where to store the binaries
target_folder="${project_root}bin"

src_folder="${project_root}src"
lib_folder="${project_root}lib"

# Name of the executable file
target_bin=sonario

# Variables for production
# prod_lib := $(shell find lib/internal -name "*.cpp" ! -name "*test.cpp") main.cpp

# Variables for testing
#   Google test
googletest_path = "${project_root}/test/googletest/googletest"
googletest_libs = $(googletest_path)/libgtest.a $(googletest_path)/libgtest_main.a

#   CppCheck
checker=/usr/local/Cellar/cppcheck/1.71/bin/cppcheck

test_lib := main_test.cpp
# sql_cipher = -Ilib/external/sqlcipher/src -Llib/external/sqlcipher -lsqlcipher

# Compiler flags
flags=-std=c++11 -Wall -Werror -g -v -I$(lib_folder)
# Compilation instructions
all:
	cd $(project_root) && shopt -s nullglob

	# Delete the executable if it exists
	if [ -a $(target_folder)/${target_bin} ] ;  then  rm $(target_folder)/${target_bin} ;  fi;


	$(compiler) ${flags} $(src_folder)/main.cpp -o $(target_folder)/${target_bin}
test:
	# $(compiler) -Wall -std=c++0x main.cpp -o $(target_folder)/${target_bin}
	cd $(project_root)
	$(compiler) \
		$(test_lib) \
		$(flags) \
		-o $(target_folder)/test
		# $(googletest_libs) \
		# $(sql_cipher)
	$(debugger) $(target_folder)/test
	# $(checker) -q main_test.cpp $(all_lib)
	# ./bin/test
clean:
	rm -f $(target_folder)/* *.o
	rm -f $(target_folder)/test
	rm -f $(target_folder)/${target_bin}
