#include <iostream>
#include "param.cpp"
#include "options.cpp"

int main(int argc, char *argv[]) {
    // I dont know if we will be dealing with accentuated characters so...
    std::locale::global(std::locale("sv_SE"));
    Param words_file = Param();
    words_file.setDescription("Tells which file stores list of words to use.");
    words_file.setLongForm("words-file");

    Param source_anagram = Param();
    source_anagram.setDescription("The orginal string to use as base.");
    source_anagram.setLongForm("source-anagram");

    Param md5_to_find = Param();
    md5_to_find.setDescription("The md5 to find.");
    md5_to_find.setLongForm("md5-to-find");


    Options main_options = Options();
    main_options.addParam(words_file);
    main_options.addParam(source_anagram);
    main_options.addParam(md5_to_find);

    std::vector<std::string> arguments(argv + 1, argv + argc);
    main_options.registerArgs(arguments);



    if (main_options.areParamsOk()) {
        if (main_options.hasCommands()) {
            main_options.runCommands();
        } else {
        }
    }


    std::cout << "Sonario test";
    return 0;
}